Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: IQ-TREE
Upstream-Contact: Lam Tung Nguyen <tung.nguyen@univie.ac.at>,
                  Bui Quang Minh <minh.bui@univie.ac.at>
Source: https://github.com/Cibiv/IQ-TREE
Files-Excluded: lib*/*
                zlib-*
                Eigen
                .*
                */.*

Files: *
Copyright: 2013-2018 Lam Tung Nguyen <tung.nguyen@univie.ac.at>
                     Heiko A. Schmidt <heiko.schmidt@mfpl.ac.at>
                     Arndt von Haeseler <arndt.von.haeseler@mfpl.ac.at>
                     Bui Quang Minh <minh.bui@mfpl.ac.at>
License: GPL-2+

Files: utils/gzstream.*
Copyright: 2001  Deepak Bandyopadhyay, Lutz Kettner
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU Lesser General
 Public License at /usr/share/common-licenses/LGPL-2.1.

Files: lbfgsb/*
Copyright: 2014 Commonwealth Scientific and Industrial Research Organisation (CSIRO) ABN 41 687 119 230
License: GPL-3+_with_additional_terms
 License version 3 as published by the Free Software Foundation (http://www.gnu.org/licenses/gpl.html), except
 where otherwise indicated for third party material.
 The following additional terms apply under clause 7 of that license:
 EXCEPT AS EXPRESSLY STATED IN THIS AGREEMENT AND TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, THE SOFTWARE
 IS PROVIDED "AS-IS". CSIRO MAKES NO REPRESENTATIONS, WARRANTIES OR CONDITIONS OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO ANY REPRESENTATIONS, WARRANTIES OR CONDITIONS REGARDING THE CONTENTS OR ACCURACY
 OF THE SOFTWARE, OR OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, THE ABSENCE
 OF LATENT OR OTHER DEFECTS, OR THE PRESENCE OR ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE.
 TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL CSIRO BE LIABLE ON ANY LEGAL THEORY (INCLUDING,
 WITHOUT LIMITATION, IN AN ACTION FOR BREACH OF CONTRACT, NEGLIGENCE OR OTHERWISE) FOR ANY CLAIM, LOSS, DAMAGES
 OR OTHER LIABILITY HOWSOEVER INCURRED.  WITHOUT LIMITING THE SCOPE OF THE PREVIOUS SENTENCE THE EXCLUSION OF
 LIABILITY SHALL INCLUDE: LOSS OF PRODUCTION OR OPERATION TIME, LOSS, DAMAGE OR CORRUPTION OF DATA OR RECORDS;
 OR LOSS OF ANTICIPATED SAVINGS, OPPORTUNITY, REVENUE, PROFIT OR GOODWILL, OR OTHER ECONOMIC LOSS; OR ANY SPECIAL,
 INCIDENTAL, INDIRECT, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES, ARISING OUT OF OR IN CONNECTION WITH THIS
 AGREEMENT, ACCESS OF THE SOFTWARE OR ANY OTHER DEALINGS WITH THE SOFTWARE, EVEN IF CSIRO HAS BEEN ADVISED OF
 THE POSSIBILITY OF SUCH CLAIM, LOSS, DAMAGES OR OTHER LIABILITY.
 APPLICABLE LEGISLATION SUCH AS THE AUSTRALIAN CONSUMER LAW MAY APPLY REPRESENTATIONS, WARRANTIES, OR CONDITIONS,
 OR IMPOSES OBLIGATIONS OR LIABILITY ON CSIRO THAT CANNOT BE EXCLUDED, RESTRICTED OR MODIFIED TO THE FULL EXTENT
 SET OUT IN THE EXPRESS TERMS OF THIS CLAUSE ABOVE "CONSUMER GUARANTEES".  TO THE EXTENT THAT SUCH CONSUMER
 GUARANTEES CONTINUE TO APPLY, THEN TO THE FULL EXTENT PERMITTED BY THE APPLICABLE LEGISLATION, THE LIABILITY
 OF CSIRO UNDER THE RELEVANT CONSUMER GUARANTEE IS LIMITED (WHERE PERMITTED AT CSIRO’S OPTION) TO ONE OF FOLLOWING
 REMEDIES OR SUBSTANTIALLY EQUIVALENT REMEDIES:
  (a)               THE REPLACEMENT OF THE SOFTWARE, THE SUPPLY OF EQUIVALENT SOFTWARE, OR SUPPLYING RELEVANT
                    SERVICES AGAIN;
  (b)               THE REPAIR OF THE SOFTWARE;
  (c)               THE PAYMENT OF THE COST OF REPLACING THE SOFTWARE, OF ACQUIRING EQUIVALENT SOFTWARE, HAVING THE
                    RELEVANT SERVICES SUPPLIED AGAIN, OR HAVING THE SOFTWARE REPAIRED.
 IN THIS CLAUSE, CSIRO INCLUDES ANY THIRD PARTY AUTHOR OR OWNER OF ANY PART OF THE SOFTWARE OR MATERIAL DISTRIBUTED
 WITH IT.  CSIRO MAY ENFORCE ANY RIGHTS ON BEHALF OF THE RELEVANT THIRD PARTY.
 Third Party Components
 The following third party components are distributed with the Software.  You agree to comply with the license
 terms for these components as part of accessing the Software.  Other third party software may also be identified
 in separate files distributed with the Software.
 .
 R : A Computer Language for Statistical Data Analysis version 3.0.1 (http://cran.r-project.org/src/base/R-3/R-3.0.1.tar.gz)
 Copyright (C) 2000-2004 The R Core Team
 This software is licensed under GNU GPL
 .
 JACOBI_EIGENVALUE.C (http://people.sc.fsu.edu/~jburkardt/c_src/jacobi_eigenvalue/jacobi_eigenvalue.c)
  Copyright (C) 2003-2013 John Burkardt
 This software is licensed under GNU LGPL (http://www.gnu.org/licenses/lgpl.html)

Files: ncl/*
Copyright: 1999-2003 Paul O. Lewis
License: GPL-2+

Files: pll/*
Copyright: 2013 Tomas Flouri and Alexandros Stamatakis
License: GPL-3+

Files: sprng/*
Copyright: 2000 Mascagni, Michael and Srinivasan, Ashok
License: NCSA
 Disclaimer: NCSA expressly disclaims any and all warranties, expressed
 or implied, concerning the enclosed software.  The intent in sharing
 this software is to promote the productive interchange of ideas
 throughout the research community. All software is furnished on an
 "as is" basis. No further updates to this software should be
 expected. Although this may occur, no commitment exists. The authors
 certainly invite your comments as well as the reporting of any bugs.
 NCSA cannot commit that any or all bugs will be fixed.
Comment: The license statement was obtained from the libsprng2 package
 of Debian.  The code here is an incompatible copy of this library.
 Upstream is informed and will enable using the Debian packaged version
 in the next release of iqtree.

Files: vectorclass/*
Copyright: 2012-2014 Agner Fog
License: GPL-3+

Files: whtest/*
Copyright: 2009 Gunter Weiss, BUI Quang Minh, Arndt von Haeseler
License: GPL-2+

Files: debian/*
Copyright: 2015-2018 Andreas Tille <tille@debian.org>
License: GPL-2+

Files: debian/Documents_source/*.tex
Copyright: 2013-2015 Lam Tung Nguyen <tung.nguyen@univie.ac.at>
                     Heiko A. Schmidt <heiko.schmidt@mfpl.ac.at>
                     Arndt von Haeseler <arndt.von.haeseler@mfpl.ac.at>
                     Bui Quang Minh <minh.bui@mfpl.ac.at>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 2 or later at `/usr/share/common-licenses/GPL-2`.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 3 or later at `/usr/share/common-licenses/GPL-3`.

